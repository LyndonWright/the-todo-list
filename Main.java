/**********************************************
 * Program: The Todo List
 * Files:
 * - Main.java
 * - Task.java
 * - ToDoList.java
 * - User.java
 * Author: Lyndon Wright
 * Course: CIDS 235-01
 * Assignment: Assignment 3 - The Todo List
 * Date Created: 11/17/2023
 * Date Modified: 11/24/2023
 * Description: This class demonstrate the
 * functionality of the Todo List application,
 * creates tasks and adds them to the todo list,
 * creates a user and associates the todo list
 * with the user, and displays the user's
 * todo list before and after adding tasks.
 **********************************************/

class Main {
  public static void main(String[] args) {

    Task tasks = new Task(
        "Garbage",
        "Put garbage in garabage bin.",
        "12/20/2023",
        true);

    ToDoList todo = new ToDoList();

    // Task
    System.out.println("Task:");
    tasks.getTask();
    tasks.getTaskDesc();
    tasks.getDueDate();
    tasks.getTaskStatus();

    // ToDoList
    System.out.println("ToDoList:");
    todo.addTask("task");
    todo.addTask("task");
    todo.displayToDoList();

    // User
    System.out.println("User:");
    User user = new User();

    // Before Tasks
    user.displayUserList();

    // user = todo;

    // After Tasks
    user.displayUserList();

  }
}