/**********************************************
 * Program: The Todo List
 * Files:
 * - Main.java
 * - Task.java
 * - ToDoList.java
 * - User.java
 * Author: Lyndon Wright
 * Course: CIDS 235-01
 * Assignment: Assignment 3 - The Todo List
 * Date Created: 11/17/2023
 * Date Modified: 11/24/2023
 * Description: This class creates data fields
 * for the Main class to use to create a task
 * and the details of that task.
 **********************************************/

class Task {

  private String task;
  private String taskDesc;
  private String dueDate;
  private boolean taskStatus;

  public Task(
      String task,
      String taskDesc,
      String dueDate,
      boolean taskStatus) {
    this.task = task;
    this.taskDesc = taskDesc;
    this.dueDate = dueDate;
    this.taskStatus = taskStatus;
  }

  public String getTask() {
    System.out.println(task);
    return task;
  }

  public String getTaskDesc() {
    System.out.println(taskDesc);
    return taskDesc;
  }

  public String getDueDate() {
    System.out.println(dueDate);
    return dueDate;
  }

  public boolean getTaskStatus() {
    if (taskStatus == true) {
      System.out.println("Task is finished.");
    } else {
      System.out.println("Task is unfinished.");
    }

    System.out.println(taskStatus);

    return taskStatus;
  }

}