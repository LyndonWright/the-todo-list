
/*************************************************
 * Program: The Todo List
 * Files: 
 * - Main.java
 * - Task.java
 * - ToDoList.java
 * - User.java
 * Author: Lyndon Wright
 * Course: CIDS 235-01
 * Assignment: Assignment 3 - The Todo List
 * Date Created: 11/17/2023
 * Date Modified: 11/24/2023
 * Description: This class shows an 
 * aggregation of tasks, implements a method 
 * in order to add a task to the list, and
 * implements a method to display all the tasks.
 *************************************************/

import java.util.ArrayList;

class ToDoList {

  private ArrayList<String> list = new ArrayList<>();

  public void addTask(String task) {
    list.add(task);
  }

  public void displayToDoList() {
    System.out.println(list);
  }

}