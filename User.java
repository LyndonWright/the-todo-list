
/**********************************************
 * Program: The Todo List
 * Files: 
 * - Main.java
 * - Task.java
 * - ToDoList.java
 * - User.java
 * Author: Lyndon Wright
 * Course: CIDS 235-01
 * Assignment: Assignment 3 - The Todo List
 * Date Created: 11/17/2023
 * Date Modified: 11/24/2023
 * Description: This class represents a user 
 * who owns a list, associates a todo list 
 * with the user using composition, and 
 * implement a method to display 
 * the user's todo list
 **********************************************/

import java.util.ArrayList;

class User {

  private ToDoList todoList;

  public void displayUserList() {
    System.out.println(todoList);
  }

}